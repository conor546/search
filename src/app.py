from flask import Flask
from flask import Response
from flask import request
from flask import render_template
import flask
import json
import pymysql.cursors
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():

    connection = pymysql.connect(unix_socket='/cloudsql/assignment-3-264920:europe-west1:conor-sql',
                                 user='root',
                                 password='Baratheon97',
                                 db='AssignmentDB')

    if request.method == "POST":
        results = request.form['find']
        # search
        try:
            with connection.cursor() as cursor:
                cursor.execute("SELECT Title, Data from scraper_data WHERE Title LIKE %s", ("%" + results + "%"))
            with connection.cursor() as cursor2:
                cursor2.execute("SELECT Text from adverts WHERE Name LIKE %s", ("%" + results + "%"))
            connection.commit()
        except Exception as e:
            logger.exception(e)
            return Response(status=500, response="Failed2")
        finally:
            data = cursor.fetchall()
            adverts = cursor2.fetchall()
            connection.close()
        # all in the search box will return all the tuples
        return render_template('home.html', data=data, adverts=adverts)
    return render_template('home.html')


#app.route('/results/')
#def results():

    #return render_template('results.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
